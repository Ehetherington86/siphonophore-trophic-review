Count	Predator	Prey	Pred_order	Siph_suborder	Prey	Prey_order	
							
1	Nanomia bijuga	chaetognath	Siphonophorae	Physonectae	Chaetognatha	Chaetognatha	
							
3	Apolemia	medusa general	Siphonophorae	Physonectae	Medusae	Medusae	
							
1	siphonophore	siphonophore	Siphonophorae		Siphonophorae	Siphonophorae	
							
1	physonect siphonophore	tunicate	Siphonophorae	Physonectae	Bathochordaeus sp. a	Appendicularia	
							
2	Bargmannia	cephalopod	Siphonophorae	Physonectae	Cephalopoda	Cephalopod	
							
1	physonect siphonophore	cephalopod	Siphonophorae	Physonectae	Galiteuthis	Cephalopod	
							
1	physonect siphonophore	copepod	Siphonophorae	Physonectae	Copepoda	Crustacean	Copepoda
							
1	Praya dubia	copepod	Siphonophorae	Calycophorae	Copepoda	Crustacean	Copepoda
							
1	Bargmannia	crustacean	Siphonophorae	Physonectae	Crustacea	Crustacean	
							
3	Lychnagalma utricularia	crustacean	Siphonophorae	Physonectae	Crustacea	Crustacean	
							
1	Nanomia bijuga	crustacean	Siphonophorae	Physonectae	Crustacea	Crustacean	
							
1	siphonophore	crustacean	Siphonophorae		Decapoda	Crustacean	Decapod
							
3	Nanomia bijuga	crustacean	Siphonophorae	Physonectae	Euphausia	Crustacean	Euphausiacea
							
3	Apolemia	crustacean	Siphonophorae	Physonectae	Euphausiacea	Crustacean	Euphausiacea
							
3	Forskalia	crustacean	Siphonophorae	Physonectae	Euphausiacea	Crustacean	Euphausiacea
							
64	Nanomia bijuga	crustacean	Siphonophorae	Physonectae	Euphausiacea	Crustacean	Euphausiacea
							
1	physonect siphonophore	crustacean	Siphonophorae	Physonectae	Euphausiacea	Crustacean	Euphausiacea
							
2	Praya dubia	crustacean	Siphonophorae	Calycophorae	Euphausiacea	Crustacean	Euphausiacea
							
3	siphonophore	crustacean	Siphonophorae		Euphausiacea	Crustacean	Euphausiacea
							
5	Nanomia bijuga	crustacean	Siphonophorae	Physonectae	Euphausiidae	Crustacean	Euphausiacea
							
1	Forskalia	crustacean	Siphonophorae	Physonectae	Sergestes	Crustacean	Decapod
							
5	Lychnagalma utricularia	crustacean	Siphonophorae	Physonectae	Sergestes	Crustacean	Decapod
							
1	Nanomia bijuga	crustacean	Siphonophorae	Physonectae	Sergestes	Crustacean	Decapod
							
1	Apolemia	ctenophore	Siphonophorae	Physonectae	Ctenophora	Ctenophore	
							
1	Praya dubia	ctenophore	Siphonophorae	Calycophorae	Ctenophora	Ctenophore	
							
2	siphonophore	ctenophore	Siphonophorae		Ctenophora	Ctenophore	
							
1	calycophoran siphonophore	cyddipid	Siphonophorae	Calycophorae	Cydippida	Ctenophore	
							
1	physonect siphonophore	cyddipid	Siphonophorae	Physonectae	Cydippida	Ctenophore	
							
1	Praya dubia	cyddipid	Siphonophorae	Calycophorae	Cydippida	Ctenophore	
							
1	siphonophore	cyddipid	Siphonophorae		Cydippida	Ctenophore	
							
1	Apolemia	cyddipid	Siphonophorae	Physonectae	Actinopteri	Fish	
							
1	Bathyphysa conifera	fish	Siphonophorae	Cystonecta	Actinopteri	Fish	
							
4	physonect siphonophore	fish	Siphonophorae	Physonectae	Actinopteri	Fish	
							
1	Stephanomia amphytridis	fish	Siphonophorae	Physonectae	Actinopteri	Fish	
							
1	Apolemia	fish	Siphonophorae	Physonectae	Cyclothone	Fish	
							
1	physonect siphonophore	fish	Siphonophorae	Physonectae	Stenobrachius leucopsarus	Fish	
							
1	Bathyphysa conifera	fish	Siphonophorae	Cystonecta	Tarletonbeania	Fish	
							
3	Apolemia	narcomedusa	Siphonophorae	Physonectae	Aegina	Hydrozoa	Narcomedusae
							
1	Desmophyes annectens	narcomedusa	Siphonophorae	Calycophorae	Aegina	Hydrozoa	Narcomedusae
							
1	Nanomia bijuga	narcomedusa	Siphonophorae	Physonectae	Aegina	Hydrozoa	Narcomedusae
							
2	physonect siphonophore	narcomedusa	Siphonophorae	Physonectae	Aegina	Hydrozoa	Narcomedusae
							
1	Apolemia	trachymedusa	Siphonophorae	Physonectae	Crossota	Hydrozoa	
							
2	Apolemia	trachymedusa	Siphonophorae	Physonectae	Haliscera conica	Hydrozoa	
							
1	Praya dubia	trachymedusa	Siphonophorae	Calycophorae	Hydromedusae	Hydrozoa	
							
1	siphonophore	medusa general	Siphonophorae		Leptomedusae	Hydrozoa	
							
1	siphonophore	narcomedusa	Siphonophorae		Narcomedusae	Hydrozoa	Narcomedusae
							
1	Apolemia	medusa general	Siphonophorae	Physonectae	Ptychogena lactea	Hydrozoa	
							
1	Apolemia	narcomedusa	Siphonophorae	Physonectae	Solmissus	Hydrozoa	
							
1	Praya dubia	narcomedusa	Siphonophorae	Calycophorae	Solmissus	Hydrozoa	
							
1	Praya dubia	trachymedusa	Siphonophorae	Calycophorae	Trachymedusae	Hydrozoa	Trachymedusae
							
2	Apolemia	scyphozoan	Siphonophorae	Physonectae	Atolla vanhoeffeni	Scyphozoa	
							
1	Praya dubia	Forskalia	Siphonophorae	Calycophorae	Forskalia	Siphonophorae	Physonectae
							
2	Praya dubia	Nanomia bijuga	Siphonophorae	Calycophorae	Nanomia bijuga	Siphonophorae	Physonectae
							
1	siphonophore	tunicate	Siphonophorae		Oikopleura	Tunicate	
							
1	Apolemia	tunicate	Siphonophorae	Physonectae	Salpida	Tunicate	
							
1	physonect siphonophore	tunicate	Siphonophorae	Physonectae	Salpida	Tunicate	
							
1	siphonophore	tunicate	Siphonophorae		Salpida	Tunicate	